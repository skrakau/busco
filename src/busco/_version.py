#!/usr/bin/env python
# coding: utf-8
"""

Copyright (c) 2016-2020, Evgeny Zdobnov (ez@ezlab.org)
Licensed under the MIT license. See LICENSE.md file.

"""
__version__ = "5.beta"
